#!/usr/bin/python3
"""Mess around with Kubernetes API"""
from os.path import expanduser
import yaml
from rest_client import RestClient

def main():
  """main"""
  kube = K8sAPIClient('minikube')
  print(kube.GET())
  print(kube.apis.extensions.GET())

class K8sAPIClient(RestClient):
  """Access Kubernetes API from .kube/config"""
  def __init__(self, context):
    self.context = context
    self.user = {}
    self.cluster = {}
    self.read_context_config()
    api_url = self.cluster['server']
    super().__init__(api_url)
    self.cert = (self.user['client-certificate'], self.user['client-key'])
    self.verify = self.cluster['certificate-authority']

  def read_context_config(self):
    """Read in config"""
    with open(expanduser('~') + '/.kube/config') as config:
      kube_config = yaml.load(config, Loader=yaml.FullLoader)
    context = [i['context'] for i in kube_config['contexts'] if i['name'] == self.context][0]
    self.user = [i['user'] for i in kube_config['users'] if i['name'] == context['user']][0]
    self.cluster = [i['cluster'] for i in kube_config['clusters'] if i['name'] == context['cluster']][0]


if __name__ == "__main__":
  main()
