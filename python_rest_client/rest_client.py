#!/usr/bin/python3
"""rest_client Class"""
import requests
import urllib.parse
from pprint import pprint


def main():
  """main"""
  # Pubic APIS: https://github.com/public-apis/public-apis/blob/master/README.md

  # https://api.openbrewerydb.org/breweries?by_state=ohio&sort=type,-name
  brew_api = RestClient('https://api.openbrewerydb.org')
  boulder_breweries = brew_api.breweries.GET(by_city='boulder', sort='type,-name').json()
  pprint(boulder_breweries)

  # Test by pulling DeathNote episodes
  anime = RestClient('https://api.jikan.moe/v3')
  death_note = anime.anime('1535').episodes.GET().json()
  pprint(death_note)


class RestClient(requests.Session):
  """Python dot calls to URL builder"""
  def __init__(self, api_url, **kwargs):
    super().__init__()
    self.api_url = api_url
    self.segments = []
    self.req_options = {}
    self.methods = ['GET', 'POST', 'PUT', 'HEAD', 'DELETE', 'PATCH', 'OPTIONS']
    self.method = ''
    self.headers.update({'Content-Type': 'application/json', 'Accept': 'application/json'})
    self.standard_query_args = {}
    self.query_args = self.standard_query_args
    if kwargs.get('auth'):
      self.auth = kwargs['auth']
    if kwargs.get('headers'):
      self.headers.update(kwargs['headers'])

  def __getattr__(self, attr):
    """Append this method name as a segment"""
    if attr in self.methods:
      self.method = attr
    else:
      self.segments.append(attr)
    return self

  def __call__(self, *argv, **kwargs):
    """Append this call argument as segment"""
    if self.method:
      return self.api_request(*argv, **kwargs)
    for arg in argv:
      self.segments.append(str(arg))
    return self

  def api_request(self, *argv, **kwargs):
    """Perform request"""
    url = self.api_url + '/' + '/'.join(self.segments)
    if kwargs:
      self.query_args.update(kwargs)
    if self.query_args:
      url += '?' + urllib.parse.urlencode(self.query_args)
    req_options = {}
    if argv:
      req_options['data'] = argv[0]
    req = requests.Request(self.method, url, **req_options)
    req = self.prepare_request(req)
    self.query_args = self.standard_query_args
    self.segments = []
    self.method = ''
    return self.send(req)
    # try:
    #   ret = self.send(req)
    #   ret.raise_for_status()
    # except requests.exceptions.HTTPError:
    #   raise
    # return ret.json()


if __name__ == "__main__":
  main()
