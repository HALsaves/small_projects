# Generic Python Rest Client

Python REST API client with intuitive call structure.  This class should work with most REST APIs unaltered.  It will rely on the server to validate the call.

If you have the server documentation, then you know how to create calls using this class.

Sample server documentation calls translated to RestClient calls
---

**Server Documentation:** `GET /organizations/{orgId}/members/{memberId}`

**RestClient call:**      *{instance}*`.organizations(orgId)/members(memberId).GET()`


**Server Documentation:** `PUT /regions/{region}/servers/{server}?tag=123`

**RestClient call:**   *{instance}*`.regions(region)/servers().PUT(tag='123')`

Sample code
---

    api = RestClient('https://myapi.org/api/v2')
    orgId = 'abc'
    memberId = 123
    data = api.organizations(orgId)/members(memberId).GET()
