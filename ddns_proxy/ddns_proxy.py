#!/usr/bin/python3
""" Update dynamic DNS from CenturyLInk Router"""
import sys
from flask import Flask, request, make_response
import requests

app = Flask(__name__) # pylint: disable=invalid-name

def main():
  """main"""
  app.run(host='0.0.0.0', port=80)

@app.route('/nic/update', methods=['GET', 'POST'])
def rpi_proxy_ddns():
  """send request to google domains with basic authentication extract hostname and ip"""
  hostip = request.args.get('myip')
  hostname = request.args.get('hostname')
  ddns_url = 'domains.google.com/nic/update'
  #extract credentials
  username = request.authorization['username']
  password = request.authorization['password']
  #build and send request
  req_url = 'https://' + username + ':' + password + '@' + ddns_url + '?hostname=' + hostname + '&myip=' + hostip
  print(req_url, file=sys.stderr)
  resp = requests.get(req_url)
  print(resp.text, resp.status_code, file=sys.stderr)
  return make_response(resp.text, resp.status_code)

if __name__ == '__main__':
  main()
