#!/bin/bash
default=$(ip route | grep '^default' | cut -f3 -d' ')
grep '^ *remote ' /etc/openvpn/target.ovpn \
| while read junk1 ip junk2
do
   ip route add ${ip}/32 via ${default} dev eth0
done
ip route add 192.168.233.0/24 via ${default} dev eth0
ip route del default
