# QBittorrent and PIA VPN containerized and podized

Note: The current PIA container (gluetun) is not working for a forwarded port.  Investigating.

docker-pia:  Create the PIA VPN container prompting for credentials
docker-qbt:  Create the QBittorrent container
qbt-start:   Start QBittorrent modifying the port to match the forwarded port from PIA
