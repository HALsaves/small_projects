#!/usr/bin/env python3
"""Calc payoff of two loans"""

# import math


def calculate_payments(loan1, loan2, payment):
    month = 0
    total_interest1 = 0
    total_interest2 = 0

    while loan1["amount"] > 0 or loan2["amount"] > 0:
        # Calculate interest for each loan
        interest1 = (loan1["amount"] * (loan1["rate"] / 100)) / 12
        interest2 = (loan2["amount"] * (loan2["rate"] / 100)) / 12

        total_interest1 += interest1
        total_interest2 += interest2

        # Apply payments
        if loan1["amount"] > 0 and loan2["amount"] > 0:
            if loan1["rate"] > loan2["rate"]:
                # Pay minimum on loan 2 and rest on loan 1
                payment1 = payment - loan2["min_payment"]
                payment2 = loan2["min_payment"]
            else:
                # Pay minimum on loan 1 and rest on loan 2
                payment1 = loan1["min_payment"]
                payment2 = payment - loan1["min_payment"]
        elif loan1["amount"] > 0:
            # All payment goes to loan 1
            payment1 = payment
            payment2 = 0
        else:
            # All payment goes to loan 2
            payment1 = 0
            payment2 = payment

        # Deduct payments from loan amounts, accounting for interest
        loan1["amount"] = max(0, loan1["amount"] + interest1 - payment1)
        loan2["amount"] = max(0, loan2["amount"] + interest2 - payment2)

        month += 1

    print(f"Total months to pay off both loans: {month}")
    print(f"Total interest paid on Loan 1: ${total_interest1:.2f}")
    print(f"Total interest paid on Loan 2: ${total_interest2:.2f}")


loan1 = {
    "amount": 150000,  # Input loan 1 amount
    "rate": 6.0,  # Input loan 1 annual interest rate in %
    "min_payment": 300,  # Input minimum payment for loan 1
}

loan2 = {
    "amount": 170000,  # Input loan 2 amount
    "rate": 2.75,  # Input loan 2 annual interest rate in %
    "min_payment": 3200,  # Input minimum payment for loan 2
}

payment = 5000  # Input combined monthly payment

calculate_payments(loan1, loan2, payment)
